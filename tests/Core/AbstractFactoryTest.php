<?php

namespace tests\Core;

use Kisphp\Core\AbstractFactory;
use PHPUnit\Framework\TestCase;

class AbstractFactoryTest extends TestCase
{
    public function test_createMysqlImportCommand()
    {
        $mysqlImportCommand = AbstractFactory::createMysqlImportCommand('demo_database', 'demo.sql');

        $params = AbstractFactory::getParameters();
        $cmd = sprintf('/usr/bin/mysql -h%s -u%s -p%s demo_database < demo.sql', $params['database_host'], $params['database_user'], $params['database_pass']);

        self::assertSame($cmd, $mysqlImportCommand);
    }

    public function test_createMysqlExportCommand()
    {
        $mysqlImportCommand = AbstractFactory::createMysqlExportCommand('demo_database', 'demo.sql');

        $params = AbstractFactory::getParameters();
        $cmd = sprintf('/usr/bin/mysqldump -h%s -u%s -p%s demo_database > demo.sql', $params['database_host'], $params['database_user'], $params['database_pass']);

        self::assertSame($cmd, $mysqlImportCommand);
    }
}
