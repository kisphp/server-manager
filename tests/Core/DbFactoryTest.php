<?php

namespace tests\Core;

use Kisphp\Core\AbstractFactory;
use PHPUnit\Framework\TestCase;
use tests\Core\Helpers\DummyKisdbFactory;
use Twig\Environment;

class DbFactoryTest extends TestCase
{
    /**
     * @return void
     * @throws \Doctrine\DBAL\Exception
     */
    public function test_success_connection()
    {
        /** @var  $db */
        $db = DummyKisdbFactory::createDatabaseConnection();

        self::assertFalse($db->getConnection()->isConnected());
    }

    public function test_twig_instantiation()
    {
        $twig = AbstractFactory::createTwig();

        self::assertInstanceOf(Environment::class, $twig);
    }
}
