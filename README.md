Kisphp Web Server Manager
=========================

> This repository was created to help with development environments. Use on production on your own risk !!!

[![pipeline status](https://gitlab.com/kisphp/server-manager/badges/main/pipeline.svg)](https://gitlab.com/kisphp/server-manager/commits/main)
[![coverage report](https://gitlab.com/kisphp/server-manager/badges/main/coverage.svg)](https://gitlab.com/kisphp/server-manager/commits/main)

The purpose of this repository is to allow an easy management of database and web server.


## Commands:

| Command | Shortcut | Description |
| ------- | -------- | ----------- |
| `app/console db:list` | `app/console d:l` | list databases and permissions |
| `app/console db:create database_name [database_user] [database_pass]` | `app/console d:c` | create database and user |
| `app/console db:drop database_name` | `app/console d:d` | drop database and user |
| `app/console nginx:activate dev.local` | `app/console n:a dev.local` | enable nginx dev.local website |
| `app/console nginx:deactivate dev.local` | `app/console n:d dev.local` | disable nginx dev.local website |
| `app/console` | `app/console` | list available commands |
