.PHONY: test install build run

install:
	composer install --no-interaction

test:
	vendor/bin/phpunit --coverage-clover=coverage.xml --coverage-html=_cov
	vendor/bin/php-cs-fixer fix --dry-run

build:
	docker build -f docker/Dockerfile -t server-manager .

run:
	docker run --rm -v $(shell pwd):/app -w /app -p 8000:80 -p 8443:443 -it server-manager bash
