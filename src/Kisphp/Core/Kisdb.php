<?php

namespace Kisphp\Core;

use Kisphp\Db\KisphpDbal;


class Kisdb extends KisphpDbal
{
    /**
     * @return bool
     */
    public function isConnected()
    {
        return (bool) $this->connection->isConnected();
    }
}
