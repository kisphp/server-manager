<?php

namespace Kisphp\Core;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Kisphp\Db\KisphpDbal;
use PDO;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class AbstractFactory
{
    const DATABASE_HOST = 'database_host';
    const DATABASE_USER = 'database_user';
    const DATABASE_PASS = 'database_pass';

    /**
     * @return KisphpDbal
     * @throws \Doctrine\DBAL\Exception
     */
    public static function createDatabaseConnection()
    {
        $config = new Configuration();

        $fileParams = static::getParameters();

        $params = [
            'driver' => 'pdo_mysql',
            'host' => $fileParams[self::DATABASE_HOST],
            'user' => $fileParams[self::DATABASE_USER],
            'password' => $fileParams[self::DATABASE_PASS],
            'charset' => 'utf8',
            'driverOptions' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ]
        ];

        $con = DriverManager::getConnection($params, $config);

        return new KisphpDbal($con);
    }

    /**
     * @return Environment
     */
    public static function createTwig()
    {
        $loader = new FilesystemLoader(static::getRootPath() . '/app/Resources/templates/');

        return new Environment($loader, [
            'cache' => false,
        ]);
    }

    /**
     * @return array
     */
    public static function getParameters()
    {
        $configContent = static::getConfigParameters();
        $config = Yaml::parse($configContent);

        return $config['parameters'];
    }

    /**
     * @param string $dbName
     * @param string $filename
     *
     * @return string
     */
    public static function createMysqlImportCommand($dbName, $filename)
    {
        $params = static::getParameters();

        $command = '/usr/bin/mysql -h' . $params[static::DATABASE_HOST];
        $command .= ' -u' . $params[static::DATABASE_USER];
        if (!empty($params[static::DATABASE_PASS])) {
            $command .= ' -p' . $params[static::DATABASE_PASS];
        }
        $command .= ' ' . $dbName;
        $command .= ' < ' . $filename;

        return $command;
    }

    /**
     * @param string $dbName
     * @param string $filename
     *
     * @return string
     */
    public static function createMysqlExportCommand($dbName, $filename)
    {
        $params = static::getParameters();

        $command = '/usr/bin/mysqldump -h' . $params[static::DATABASE_HOST];
        $command .= ' -u' . $params[static::DATABASE_USER];
        if (!empty($params[static::DATABASE_PASS])) {
            $command .= ' -p' . $params[static::DATABASE_PASS];
        }
        $command .= ' ' . $dbName;
        $command .= ' > ' . $filename;

        return $command;
    }

    /**
     * @return string
     */
    protected static function getRootPath()
    {
        return realpath(__DIR__ . '/../../../');
    }

    /**
     * @return string
     */
    protected static function getConfigParameters()
    {
        $parametersPath = (static::getRootPath() . '/app/config/parameters.yml');

        return file_get_contents($parametersPath);
    }
}
