<?php

namespace Kisphp\Command\Database;

use Kisphp\Core\AbstractFactory;
use Kisphp\Services\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{
    const DB_NAME = 'name';
    const DB_FILE = 'file';

    const DESCRIPTION = 'Import database schema';
    const COMMAND = 'db:import';

    /**
     * @var Database
     */
    protected $db;

    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription(self::DESCRIPTION)
            ->addArgument(self::DB_NAME, InputArgument::REQUIRED, 'Database name')
            ->addArgument(self::DB_FILE, InputArgument::REQUIRED, 'Schema file')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>' . self::DESCRIPTION . '</info>');

        $dbName = $input->getArgument(self::DB_NAME);
        $filename = $input->getArgument(self::DB_FILE);

        $this->db = AbstractFactory::createDatabaseConnection();

        if ($this->db === false) {
            return self::FAILURE;
        }

        $command = AbstractFactory::createMysqlImportCommand($dbName, $filename);

        shell_exec($command);

        $output->writeln(sprintf('Imported database <info>%s</info>', $dbName));

        return self::SUCCESS;
    }
}
