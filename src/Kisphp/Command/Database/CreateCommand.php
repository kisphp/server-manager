<?php

namespace Kisphp\Command\Database;

use Kisphp\Core\AbstractFactory;
use Kisphp\Services\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    const DB_NAME = 'name';
    const DB_USER = 'user';
    const DB_PASS = 'pass';

    const DESCRIPTION = 'Create database and user';
    const COMMAND = 'db:create';

    /**
     * @var Database
     */
    protected $db;

    /**
     * @var string
     */
    protected $hostname = 'localhost';

    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription(self::DESCRIPTION)
            ->addOption('server', 's', InputOption::VALUE_OPTIONAL, 'Server hostname')
            ->addArgument(self::DB_NAME, InputArgument::REQUIRED, 'Database name')
            ->addArgument(self::DB_USER, InputArgument::OPTIONAL, 'Database username')
            ->addArgument(self::DB_PASS, InputArgument::OPTIONAL, 'Database password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>' . self::DESCRIPTION . '</info>');

        $hostnameOption = $input->getOption('server');
        if ( ! empty($hostnameOption)) {
            $this->hostname = $hostnameOption === 'all' || $hostnameOption === '%' ? '%%' : $hostnameOption;
        }

        $dbName = $input->getArgument(self::DB_NAME);
        $dbUser = $input->getArgument(self::DB_USER);
        if (empty($dbUser)) {
            $dbUser = $dbName;
        }
        $dbPass = $input->getArgument(self::DB_PASS);
        if (empty($dbPass)) {
            $dbPass = $dbName;
        }

        $this->db = AbstractFactory::createDatabaseConnection();

        if ($this->db === false) {
            return self::FAILURE;
        }

        $createDb = $this->createDatabase($output, $dbName);
        if ($createDb === false) {
            return self::FAILURE;
        }
        $createUser =  $this->createUser($output, $dbUser, $dbPass);
        if ($createUser === false) {
            return self::FAILURE;
        }
        $grantAccess = $this->grantUserAccess($output, $dbName, $dbUser);
        if ($grantAccess === false) {
            return self::FAILURE;
        }

        return self::SUCCESS;
    }

    /**
     * @param OutputInterface $output
     * @param string $databaseName
     */
    protected function createDatabase(OutputInterface $output, $databaseName)
    {
        try {
            $query = sprintf('CREATE DATABASE IF NOT EXISTS %s', $databaseName);
            $this->db->query($query);

            if ($output->isVerbose()) {
                $output->writeln($query);
            }

            return true;
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return false;
        }
    }

    /**
     * @param OutputInterface $output
     * @param string $user
     * @param string $password
     */
    protected function createUser(OutputInterface $output, $user, $password)
    {
        try {
            $query = sprintf("CREATE USER IF NOT EXISTS '%s'@'%s' IDENTIFIED BY '%s'", $user, $this->hostname, $password);
            $this->db->query($query);

            if ($output->isVerbose()) {
                $output->writeln($query);
            }
            return true;
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return false;
        }
    }

    /**
     * @param OutputInterface $output
     * @param string $databaseName
     * @param string $user
     */
    protected function grantUserAccess(OutputInterface $output, $databaseName, $user)
    {
        try {
            $query = sprintf("GRANT ALL PRIVILEGES ON %s.* TO '%s'@'%s'", $databaseName, $user, $this->hostname);
            $this->db->query($query);

            if ($output->isVerbose()) {
                $output->writeln($query);
            }
            return true;
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return false;
        }
    }
}
