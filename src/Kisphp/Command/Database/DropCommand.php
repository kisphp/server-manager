<?php

namespace Kisphp\Command\Database;

use Kisphp\Core\AbstractFactory;
use Kisphp\Db\KisphpDbal;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DropCommand extends Command
{
    const DB_NAME = 'name';
    const DB_USER = 'user';

    const DESCRIPTION = 'Drop database and delete user';
    const COMMAND = 'db:drop';

    /**
     * @var
     */
    protected $db;

    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription(self::DESCRIPTION)
            ->addArgument(self::DB_NAME, InputArgument::REQUIRED, 'Database name')
            ->addArgument(self::DB_USER, InputArgument::OPTIONAL, 'Database username')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>' . self::DESCRIPTION . '</info>');

        $dbName = $input->getArgument(self::DB_NAME);
        $dbUser = $input->getArgument(self::DB_USER);
        if (empty($dbUser)) {
            $dbUser = $dbName;
        }

        /** @var KisphpDbal db */
        $this->db = AbstractFactory::createDatabaseConnection(null);

        if ($this->db === false) {
            return self::FAILURE;
        }

        $this->createDatabase($output, $dbName);
        $this->dropUser($output, $dbUser);

        return self::SUCCESS;
    }

    /**
     * @param OutputInterface $output
     * @param string $databaseName
     * @return bool
     */
    protected function createDatabase(OutputInterface $output, $databaseName)
    {
        try {
            $query = sprintf('DROP DATABASE IF EXISTS %s', $databaseName);
            $this->db->query($query);

            if ($output->isVerbose()) {
                $output->writeln($query);
            }
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return false;
        }

        return true;
    }

    /**
     * @param OutputInterface $output
     * @param string $user
     * @return bool
     */
    protected function dropUser(OutputInterface $output, $user)
    {
        try {
            $query = sprintf("DROP USER '%s'@'%%'", $user);
            $this->db->query($query);

            if ($output->isVerbose()) {
                $output->writeln($query);
            }
        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return false;
        }

        return true;
    }
}
