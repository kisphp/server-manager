<?php

namespace Kisphp\Command\Database;

use Kisphp\Core\AbstractFactory;
use Kisphp\Services\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListCommand extends Command
{
    const DESCRIPTION = 'List databases and users privileges (slow)';
    const COMMAND = 'db:list';

    /**
     * @var Database
     */
    protected $db;

    /**
     * @var array
     */
    protected $grants = [];

    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription(self::DESCRIPTION)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>' . self::DESCRIPTION . '</info>');

        $this->db = AbstractFactory::createDatabaseConnection();

        $this->createGrantsTable($output);
        $this->createUsersTable($output);
        $this->createDatabasesListTable($output);

        return self::SUCCESS;
    }

    /**
     * @param OutputInterface $output
     */
    protected function createGrantsTable(OutputInterface $output)
    {
        $users = $this->db->query('SELECT `User`, `Host` FROM `mysql`.`user`');
        foreach ($users->fetchAll(\PDO::FETCH_ASSOC) as $user) {
            if ($user['User'] === 'root' || strpos($user['User'], 'mysql') !== false) {
                continue;
            }
            $this->getGrantsForUser($user);
        }

        $this->renderGrantsTable($output);
    }

    /**
     * @param string $user
     */
    protected function getGrantsForUser($user)
    {
        $sql = sprintf(
            "SHOW GRANTS for '%s'@'%s'",
            $user['User'],
            $user['Host']
        );
        $grants = $this->db->query($sql);

        while ($gr = $grants->fetch(\PDO::FETCH_NUM)) {
            $this->grants[][] = $gr[0];
        }
    }

    /**
     * @param OutputInterface $output
     */
    protected function renderGrantsTable(OutputInterface $output)
    {
        $table = new Table($output);
        $table
            ->setHeaders([
                'Grant List',
            ])
            ->setRows(
                $this->grants
            )
        ;

        $table->render();
    }

    /**
     * @param OutputInterface $output
     */
    protected function createUsersTable(OutputInterface $output)
    {
        $userList = [];
        $users = $this->db->query('SELECT `Host`, `Db`, `User` FROM `mysql`.`db`');
        while ($user = $users->fetch(\PDO::FETCH_ASSOC)) {
            $userList[] = $user;
        }

        $this->renderUsersTable($output, $userList);
    }

    /**
     * @param OutputInterface $output
     * @param array $userList
     */
    protected function renderUsersTable(OutputInterface $output, array $userList)
    {
        $table = new Table($output);
        $table
            ->setHeaders([
                'Host',
                'Database',
                'User',
            ])
            ->setRows(
                $userList
            )
        ;

        $table->render();
    }

    /**
     * @param OutputInterface $output
     */
    protected function createDatabasesListTable(OutputInterface $output)
    {
        $query = $this->db->query('SHOW DATABASES');

        $databases = [];
        while ($db = $query->fetch(\PDO::FETCH_ASSOC)) {
            $databases[] = $db;
        }

        $this->renderDatabasesTable($output, $databases);
    }

    /**
     * @param OutputInterface $output
     * @param array $databases
     */
    protected function renderDatabasesTable(OutputInterface $output, array $databases)
    {
        $table = new Table($output);
        $table
            ->setHeaders([
                'Databases List',
            ])
            ->setRows(
                $databases
            )
        ;

        $table->render();
    }
}
