<?php

namespace Kisphp\Command;

use Kisphp\Core\AbstractFactory;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SiteCreateCommand extends AbstractSiteCommander
{
    const DESCRIPTION = 'Create new site';
    const COMMAND = 'site:create';

    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription(self::DESCRIPTION)
            ->addArgument('directory', InputArgument::REQUIRED, 'Set directory name')
            ->addArgument('public_directory', InputArgument::OPTIONAL, 'Set public directory inside project', AbstractSiteCommander::PUBLIC_DIRECTORY)
            ->addOption('activate', 'a', InputOption::VALUE_NONE, 'Activate nginx or apache server configuration')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        if ($input->getOption('activate')) {
            $this->executeOnlyForRoot();
        }

        $parameters = AbstractFactory::getParameters();

        $serverPath = $parameters['server_path'];

        $projectDirectory = $serverPath . '/' . $this->input->getArgument('directory');

        if (is_dir($projectDirectory)) {
            $this->outputError('Directory ' . $this->input->getArgument('directory') . ' already exists');

            return self::FAILURE;
        }

        $this->createProjectDirectory($projectDirectory . '/' . $this->input->getArgument('public_directory'));

        $this->success('Site directory successfully created');

        return self::SUCCESS;
    }

    /**
     * @param string $projectDirectory
     *
     * @return bool
     */
    protected function createProjectDirectory($projectDirectory)
    {
        $this->comment('Create project directory: ' . $projectDirectory);

        return mkdir($projectDirectory, 0755, true);
    }
}
