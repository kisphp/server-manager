<?php

namespace Kisphp\Command\Nginx;

use Kisphp\Command\AbstractSiteCommander;
use Kisphp\Core\AbstractFactory;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ActivateCommand extends AbstractSiteCommander
{
    const COMMAND = 'nginx:activate';

    protected function configure()
    {
        $this->setName(self::COMMAND)
            ->setDescription('Activate vhost')
            ->addOption('no-restart', null, InputOption::VALUE_OPTIONAL, 'Skip server restart', false)
            ->addOption('wordpress', 'w', InputOption::VALUE_NONE, 'Configure wordpress website')
            ->addArgument('directory', InputArgument::REQUIRED, 'Set directory name')
            ->addArgument('public_directory', InputArgument::OPTIONAL, 'Set public directory inside project', self::PUBLIC_DIRECTORY)
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return null|bool|int
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $this->executeOnlyForRoot();

        $parameters = AbstractFactory::getParameters();

        $serverPath = $parameters['server_path'];
        $serverType = $parameters['server_type'];

        $isSuccess = $this->createVhost($serverPath, $serverType);

        if ($isSuccess === false) {
            return self::FAILURE;
        }

        if ($input->getOption('no-restart') === false) {
            $this->restartServer();
        }

        $this->success('Domain ' . $this->input->getArgument('directory') . ' successfully activated');

        return self::SUCCESS;
    }

    /**
     * @param string $serverPath
     * @param string $serverType
     *
     * @return bool
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function createVhost($serverPath, $serverType)
    {
        $this->comment('Create vhost: ' . $this->input->getArgument('directory'));

        $twig = AbstractFactory::createTwig();

        $directory = $this->input->getArgument('directory');
        $isWordpress = $this->input->getOption('wordpress');

        $params = [
            'server_path' => $serverPath,
            'directory' => $directory,
            'domain' => $directory,
            'public_directory' => $this->input->getArgument('public_directory'),
        ];

        $directoryPath = $serverPath . '/' . $directory;
        if (is_dir($directoryPath) === false) {
            $this->outputError($directoryPath . ' does not exists');

            return false;
        }

        if ($isWordpress) {
            $serverType = 'wordpress';
        }
        $vhost = $twig->render('nginx/vhost-' . $serverType . '.twig', $params);


        $vhostTarget = $this->getNginxVhostTarget($directory);
        $symlinkTarget = $this->getNginxSymlinkTarget($directory);

        file_put_contents($vhostTarget, $vhost);

        if (is_file($symlinkTarget)) {
            unlink($symlinkTarget);
        }

        symlink($vhostTarget, $symlinkTarget);

        return true;
    }
}
